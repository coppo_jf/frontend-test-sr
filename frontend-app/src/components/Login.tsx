import React, { useState } from 'react';
import '../styles/Login.css';
import { useNavigate } from 'react-router-dom';

async function loginUser(credentials) {
    let headers = new Headers();

    headers.append('Content-Type', 'application/json');
    headers.append('Accept', 'application/json');
    headers.append('Origin', 'http://localhost:3000/');

    return fetch('http://localhost:3000/auth', {
        mode: 'cors',
        method: 'POST',
        body: JSON.stringify(credentials),
        headers: headers
    })
        .then(data => data.json())
}

export default function Login({ setToken }) {
    const [identifiant, setIdentifiant] = useState();
    const [password, setPassword] = useState();

    const navigate = useNavigate();

    const handleSubmit = async e => {
        e.preventDefault();
        const token = await loginUser({
            identifiant,
            password
        });
        setToken(token);
        navigate('/graphs');
    }

    return (
        <div className="login-block">
            <form onSubmit={handleSubmit}>
                <ul className="form-list">
                    <li>
                        <input className="form-ph login-ph" type="text" placeholder="Username" onChange={e => setIdentifiant(e.target.value)} />
                    </li>
                    <li>
                        <input className="form-ph" type="password" placeholder="Password" onChange={e => setPassword(e.target.value)} />
                    </li>
                </ul>
                <div>
                    <button className="login-btn" type="submit">Submit</button>
                </div>
            </form>
        </div >
    );
}


