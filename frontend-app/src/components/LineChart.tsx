import React, { useState, useEffect } from 'react'
import {
    Chart as ChartJS,
    CategoryScale,
    LinearScale,
    PointElement,
    LineElement,
    Title,
    Tooltip,
    Legend,
} from 'chart.js';
import '../styles/LineChart.css';

import axios from 'axios';

import { Line } from 'react-chartjs-2';

ChartJS.register(
    CategoryScale,
    LinearScale,
    PointElement,
    LineElement,
    Title,
    Tooltip,
    Legend
);


const LineChart = () => {
    const [cdn, setCdn] = useState([]);
    const [cdngood, setCdngood] = useState([]);
    const [bandwidth, setBandwidth] = useState([]);
    const [ptp, setPtp] = useState([]);
    const [cdnmax, setCdnmax] = useState([]);
    const [ptpmax, setPtpmax] = useState([]);
    const stoken = localStorage.getItem('session_token');
    const stokenF = JSON.parse(stoken);
    const session_token = stokenF.session_token;
    const from = "1509494400000";
    const to = "1510790400000";

    useEffect(() => {
        const fetchData = async () => {
            var result = await axios.post('http://localhost:3000/bandwidth', { session_token: session_token, from: from, to: to });
            var resultmax = await axios.post('http://localhost:3000/bandwidth', { session_token: session_token, from: from, to: to, aggregate: "max" });
            const cdnarray = [];
            const cdngood = [];
            await result.data.cdn.forEach((item: any[]) => {
                const date = new Intl.DateTimeFormat('fr-FR', { year: 'numeric', month: '2-digit', day: '2-digit' }).format(item[0]);
                cdngood.push(date);
                if (!cdnarray.includes(date)) {
                    cdnarray.push(date);
                }
            })
            result.data.cdn = result.data.cdn.map(x => {
                return (
                    (x[1] / 10000000000).toFixed(2)
                )
            })
            result.data.p2p = result.data.p2p.map(x => {
                return (
                    (x[1] / 10000000000).toFixed(2)
                )
            })
            for (let i = 0; i < result.data.cdn.length; i++) {
                cdnmax.push((resultmax.data.cdn / 10000000000).toFixed(2));
                ptpmax.push((resultmax.data.p2p / 10000000000).toFixed(2));
            }
            setCdn(cdnarray);
            setCdngood(cdngood);
            setBandwidth(result.data.cdn);
            setPtp(result.data.p2p);
            setCdnmax(cdnmax);
            setPtpmax(ptpmax);
        }
        fetchData();
    }, []);

    var data = {
        labels: cdngood?.map(x => x),
        datasets: [
            {
                label: `Http in Gbps `,
                data: bandwidth?.map(x => x),
                backgroundColor: [
                    'rgba(255, 99, 132, 0)'
                ],
                borderColor: [
                    'rgba(255, 99, 132, 1)',
                ],
                borderWidth: 1,
            },
            {
                label: `P2P in Gbps`,
                data: ptp?.map(x => x),
                backgroundColor: [
                    'rgba(54, 162, 235, 0.2)',
                ],
                borderColor: [
                    'rgba(54, 162, 235, 1)',
                ],
                borderWidth: 1,
            },
            {
                label: `Http Max`,
                data: cdnmax,
                backgroundColor: [
                    'rgba(255, 99, 132, 0.2)',
                ],
                borderColor: [
                    'rgba(255, 99, 132, 1)',
                ],
                borderDash: [5, 5],
                borderWidth: 1,
            },
            {
                label: `P2P Max`,
                data: ptpmax,
                backgroundColor: [
                    'rgba(54, 162, 235, 0.2)',
                ],
                borderColor: [
                    'rgba(54, 162, 235, 1)',
                ],
                borderDash: [5, 5],
                borderWidth: 1,
            }
        ]
    };

    var options = {
        plugins: {

            legend: {
                display: false
            },
        },

        elements: {
            point: {
                radius: 0
            }
        },
        interaction: {
            mode: 'index',
            intersect: false,
        },
        maintainAspectRatio: false,
        scales: {
        },
        legend: {
            labels: {
                fontSize: 12,
            },
        },
    }

    return (
        <div className="linechart-body" >
            <div>
                <Line
                    data={data}
                    height={400}
                    options={options}
                />
            </div>
            <div style={{ display: "flex", justifyContent: "center" }}>
                <p style={{ marginRight: "20px", padding: "10px", borderRadius: "10px", backgroundColor: "rgba(255, 99, 132, 1)" }}>
                    Http
                </p>
                <p style={{ marginRight: "20px", padding: "10px", borderRadius: "10px", backgroundColor: "rgba(54, 162, 235, 1)" }}>
                    P2P
                </p>
            </div>

        </div >
    )
}

export default LineChart
