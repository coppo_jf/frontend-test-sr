import React, { useState, useEffect } from 'react'
import {
    Chart as ChartJS,
    CategoryScale,
    LinearScale,
    PointElement,
    LineElement,
    Title,
    Tooltip,
    Legend,
} from 'chart.js';
import '../styles/AudienceChart.css';

import axios from 'axios';

import { Line } from 'react-chartjs-2';

ChartJS.register(
    CategoryScale,
    LinearScale,
    PointElement,
    LineElement,
    Title,
    Tooltip,
    Legend
);

const AudienceChart = () => {
    const [timestamp, setTimestamp] = useState([]);
    const [cdngood, setCdngood] = useState([]);
    const [viewers, setViewers] = useState([]);
    const [viewersmax, setViewersmax] = useState([]);
    const stoken = localStorage.getItem('session_token');
    const stokenF = JSON.parse(stoken);
    const session_token = stokenF.session_token;
    const from = "1509494400000";
    const to = "1510790400000";

    useEffect(() => {
        const fetchData = async () => {
            var result = await axios.post('http://localhost:3000/audience', { session_token: session_token, from: from, to: to });
            var resultmax = await axios.post('http://localhost:3000/audience', { session_token: session_token, from: from, to: to, aggregate: "max" });
            const cdnarray = [];
            const cdngood = [];
            await result.data.audience.forEach((item: any[]) => {
                const date = new Intl.DateTimeFormat('fr-FR', { year: 'numeric', month: '2-digit', day: '2-digit' }).format(item[0]);
                cdngood.push(date);
                if (!cdnarray.includes(date)) {
                    cdnarray.push(date);
                }
            })
            result.data.auudience = result.data.audience.map(x => {
                return (
                    x[1]
                )
            })
            for (let i = 0; i < result.data.audience.length; i++) {
                viewersmax.push(resultmax.data.audience);
            }
            setTimestamp(cdnarray);
            setCdngood(cdngood);
            setViewers(result.data.audience);
            setViewersmax(viewersmax);
        }
        fetchData();
    }, []);

    var data = {
        labels: cdngood?.map(x => x),
        datasets: [
            {
                label: `Number of viewers `,
                data: viewers?.map((x: any) => x),
                backgroundColor: [
                    'rgba(212, 224, 33, 0)'
                ],
                borderColor: [
                    'rgba(197, 144, 30, 1)',
                ],
                borderWidth: 1,
            },
            {
                label: `Viewers Max`,
                data: viewersmax,
                backgroundColor: [
                    'rgba(197, 144, 30, 0)'
                ],
                borderColor: [
                    'rgba(197, 144, 30, 1)',
                ],
                borderDash: [5, 5],
                borderWidth: 1,
            },
        ]
    };

    var options = {
        plugins: {

            legend: {
                display: false
            },
        },

        elements: {
            point: {
                radius: 0
            }
        },
        interaction: {
            mode: 'index',
            intersect: false,
        },
        maintainAspectRatio: false,
        scales: {
        },
        legend: {
            labels: {
                fontSize: 12,
            },
        },
    }

    return (
        <div className="linechart-body" >
            <div>
                <Line
                    data={data}
                    height={400}
                    options={options}
                />
            </div>
            <div style={{ display: "flex", justifyContent: "center" }}>
                <p style={{ marginRight: "20px", padding: "10px", borderRadius: "10px", backgroundColor: "rgba(197, 144, 30, 1)" }}>
                    Viewers
                </p>
            </div>

        </div >
    )
}

export default AudienceChart
