import React, { useState } from 'react';
import { useEffect } from 'react';
import '../styles/Graphs.css';
import axios from 'axios';
import { useNavigate } from 'react-router-dom';
import LineChart from './LineChart.tsx';
import AudienceChart from './AudienceChart.tsx';

async function logoutUser() {
    const stoken = localStorage.getItem('session_token');
    const stokenF = JSON.parse(stoken);
    const session_token = stokenF.session_token;
    await axios.post('http://localhost:3000/logout', { session_token: session_token });
}

export default function Graphs() {
    const navigate = useNavigate();

    const handleSubmit = async e => {
        e.preventDefault();
        await logoutUser();
        navigate('/');
    }

    return (
        <div className="graphs-block">
            <button className="logout-btn" onClick={handleSubmit}>
                Logout
            </button>
            <h1 className="chart-title">Capacity offload</h1>
            <LineChart />
            <h1 className="chart-title">Audience</h1>
            <AudienceChart />
        </div>
    );
}
