import { render } from "react-dom";
import {
    BrowserRouter,
    Routes,
    Route,
} from "react-router-dom";

import App from "./App.tsx";
import Graphs from "./components/Graphs.tsx";

const rootElement = document.getElementById("root");
render(
    <BrowserRouter>
        <Routes>
            <Route path="/" element={<App />} />
            <Route path="/graphs" element={<Graphs />} />
        </Routes>
    </BrowserRouter>,
    rootElement
);