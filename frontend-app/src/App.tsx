import React from 'react';
import './styles/App.css';
import Login from './components/Login.tsx';
import Graphs from './components/Graphs.tsx';
import useToken from './components/UseToken.tsx';


export default function App() {

  const { token, setToken } = useToken();

  return (
    <div className="App">
      <header className="App-header">
        <h1>CDN Graph Explorer</h1>
        <h2>Please Log-in</h2>
      </header>
      <Login setToken={setToken}></Login>
    </div>
  )
}
